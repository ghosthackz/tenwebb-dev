module.exports = {
    content: ["public/index.html"],
    css: ["public/assets/css/tw.css"],
    extractors: [
        {
            extractor: class {
                static extract(content) {
                    return content.match(/[A-Za-z0-9-_:/]+/g) || []
                }
            },
            extensions: ["html", "js"]
        }
    ]
}